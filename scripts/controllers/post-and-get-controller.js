'use strict'

app.controller('PostAndGetController', PostAndGetController);

function PostAndGetController ($scope, FURL, $firebase, $location, $routeParams) {
  var ref = new Firebase(FURL);
  var fbPosts = $firebase(ref.child('posts')).$asArray();
  var postId = $routeParams.postId;

  if(postId) {
    $scope.selectedPost = getPost(postId);
  }

  $scope.posts = fbPosts;

  function getPost(postId) {
    return $firebase(ref.child('posts').child(postId)).$asObject();
  }

  $scope.postPost = function(post) {
    fbPosts.$add(post);
    $location.path('/browse');
  };

  $scope.postEdit = function(post) {
    $scope.selectedPost.$save(post);
    $location.path('/browse');
  };

  $scope.postDelete = function(post) {
    $firebase(ref.child('posts').child(post.$id)).$remove();
  };
}